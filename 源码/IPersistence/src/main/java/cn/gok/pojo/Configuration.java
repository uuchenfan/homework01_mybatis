package cn.gok.pojo;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * 专门保存用来解析出的主配置文件相关信息的实体类
 */
public class Configuration {
    //根据解析出的 数据库驱动，用户名，密码等信息，直接创建DataSource
    private DataSource dataSource;

    /**
     * 将mapper.xml 中解析出来的数据存入此map中
     *      key ： mapper配置文件中，每条sql标签对应的 statementid
     *      value： 解析出的每条sql标签的相关信息，存入到MappedStatement对象中
     */
    private Map<String,MappedStatement> mappedStatementMap = new HashMap<>();


    /**
     * 该变量用来存储解析出的mapper.xml 和  对应接口中的方法
     * 究竟是属于 SELECT、UPDATE、DELETE 、INSERT中哪一种类型的方法
     *
     *  key为statementid，  value为方法的类型
     */
    private Map<String,String> methodType = new HashMap<>();



    public Map<String, String> getMethodType() {
        return methodType;
    }

    public void setMethodType(Map<String, String> methodType) {
        this.methodType = methodType;
    }



    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Map<String, MappedStatement> getMappedStatementMap() {
        return mappedStatementMap;
    }

    public void setMappedStatementMap(Map<String, MappedStatement> mappedStatementMap) {
        this.mappedStatementMap = mappedStatementMap;
    }
}
