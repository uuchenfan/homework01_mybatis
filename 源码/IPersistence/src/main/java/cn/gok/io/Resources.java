package cn.gok.io;

import java.io.InputStream;

/**
 *专门用来读取配置文件的工具类
 */
public class Resources {
    public static InputStream getResourceAsSteam(String path){
        InputStream resourceAsStream = Resources.class.getClassLoader().getResourceAsStream(path);
        return resourceAsStream;
    }
}
