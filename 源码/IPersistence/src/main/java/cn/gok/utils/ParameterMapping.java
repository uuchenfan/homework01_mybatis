package cn.gok.utils;

/**
 *解析出的sql语句中的每一个参数都是一个ParameterMapping对象
 */
public class ParameterMapping {

    private String content;

    public ParameterMapping(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
