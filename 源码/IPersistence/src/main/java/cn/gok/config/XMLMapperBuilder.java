package cn.gok.config;

import cn.gok.pojo.Configuration;
import cn.gok.pojo.MappedStatement;
import com.mysql.jdbc.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 专门用来继续mapper.xml  配置文件的工具类
 */
public class XMLMapperBuilder {
    private Configuration configuration;
    public XMLMapperBuilder(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * 解析mapper.xml配置文件
     *      @param inputStream：
     */
    public void parse(InputStream inputStream) throws DocumentException, ClassNotFoundException {
        Document document = new SAXReader().read(inputStream);
        Element rootElement = document.getRootElement();
        String namespace = rootElement.attributeValue("namespace");

        //用来存放mapper中所有的select、update、insert、delete元素
        List<Element> sqlElement = new ArrayList<>();

        //取出所有的select、update、insert、delete元素存入sqlElement集合中
        sqlElement.addAll(rootElement.selectNodes("select"));
        sqlElement.addAll(rootElement.selectNodes("update"));
        sqlElement.addAll(rootElement.selectNodes("insert"));
        sqlElement.addAll(rootElement.selectNodes("delete"));

        for (Element element : sqlElement) {
            //获取select标签中各个属性的值
            String id = element.attributeValue("id");
            String paramterType = element.attributeValue("paramterType");
            String resultType = element.attributeValue("resultType");


            // 获取paramterType 和 resultType 对应的 class对象
            //输入参数的class
            Class<?> paramterTypeClass = null;
            if (!StringUtils.isNullOrEmpty(paramterType)){
                paramterTypeClass = getClassType(paramterType);
            }

            //返回参数的class
            Class<?> resultTypeClass = null;
            if(!StringUtils.isNullOrEmpty(resultType)){
                resultTypeClass = getClassType(resultType);
            }

            // 获取标签中的sql语句
            String textTrim = element.getTextTrim();

            //获取statement的id
            String key = namespace +"."+id;

            //将解析出来的数据封装到MappedStatement 对象中 ，解析出的每条sql标签就一个MappedStatement 对象
            MappedStatement mappedStatement = new MappedStatement();
            mappedStatement.setId(id);
            mappedStatement.setParamterType(paramterTypeClass);
            mappedStatement.setResultType(resultTypeClass);
            mappedStatement.setSql(textTrim);

            /*
                解析出来的MappedStatement对象最终再存入到Configuration 的 mappedStatementMap 属性中
                其中key即是  statementId ，    value为 mappedStatement
             */
            configuration.getMappedStatementMap().put(key,mappedStatement);

            //确认方法是增删改查中的哪一种方法
            switch (element.getName()){
                case "select":configuration.getMethodType().put(key,"SELECT");break;
                case "update":configuration.getMethodType().put(key,"UPDATE");break;
                case "insert":configuration.getMethodType().put(key,"INSERT");break;
                case "delete":configuration.getMethodType().put(key,"DELETE");
            }

        }
    }


    /**
     * 返回指定的全路径的类 的 class对象
     */
    private Class<?> getClassType(String paramterType) throws ClassNotFoundException {
        Class<?> aClass = Class.forName(paramterType);
        return aClass;
    }
}
