package cn.gok.config;

import cn.gok.utils.ParameterMapping;

import java.util.ArrayList;
import java.util.List;

public class BoundSql {

    /**
     * 解析后的sql语句
     */
    private String sqlText;

    /**
     * 解析后的参数，比如select * from t_user where user_name = #{userName} and password = #{password}
     *  那么这里解析出来的就是{}中的   userName 和 password
     *  将解析出的这些参数封装到ParameterMapping对象中。
     */
    private List<ParameterMapping> parameterMappingList = new ArrayList<ParameterMapping>();


    public BoundSql(String sqlText, List<ParameterMapping> parameterMappingList) {
        this.sqlText = sqlText;
        this.parameterMappingList = parameterMappingList;
    }


    public String getSqlText() {
        return sqlText;
    }

    public void setSqlText(String sqlText) {
        this.sqlText = sqlText;
    }

    public List<ParameterMapping> getParameterMappingList() {
        return parameterMappingList;
    }

    public void setParameterMappingList(List<ParameterMapping> parameterMappingList) {
        this.parameterMappingList = parameterMappingList;
    }
}
