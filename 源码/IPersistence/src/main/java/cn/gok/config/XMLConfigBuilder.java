package cn.gok.config;

import cn.gok.io.Resources;
import cn.gok.pojo.Configuration;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.beans.PropertyVetoException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

/**
 * 专门用来解析配置文件的工具类
 */
public class XMLConfigBuilder {

    private Configuration configuration;

    public XMLConfigBuilder() {
        this.configuration = new Configuration();
    }

    /**
     * 通过dom4j 解析配置文件，并将解析的结果封装到Configuration对象中
     *       配置文件有两种，一个是主配置文件configuration.xml，一个是映射配置文件mapper.xml
     *
     * @param inputStream
     * @return
     */
    public Configuration parseConfig(InputStream inputStream) throws DocumentException, PropertyVetoException, ClassNotFoundException {
        Document document = new SAXReader().read(inputStream);

        //-----------------1、先解析主配置文件-----------------------------------------

        // 1、获取根元素，即： <configuration>
        Element rootElement = document.getRootElement();

        // 2、获取property元素
        List<Element> list = rootElement.selectNodes("//property");

        //3、 将解析出来的数据存入Properties对象
        Properties properties = new Properties();
        for (Element element : list) {
            String name = element.attributeValue("name");
            String value = element.attributeValue("value");
            properties.setProperty(name,value);
        }

        //4、根据解析到的数据创建C3p0 连接池
        ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
        comboPooledDataSource.setDriverClass(properties.getProperty("driverClass"));
        comboPooledDataSource.setJdbcUrl(properties.getProperty("jdbcUrl"));
        comboPooledDataSource.setUser(properties.getProperty("username"));
        comboPooledDataSource.setPassword(properties.getProperty("password"));

        //将数据源存放到configuration中
        configuration.setDataSource(comboPooledDataSource);

        //-----------------------解析Mappper映射配置文件信息------------------------------------------
        List<Element> mapperElements = rootElement.selectNodes("//mapper");

        //创建解析mapper.xml的工具对象
        XMLMapperBuilder mapperBuilder = new XMLMapperBuilder(configuration);

        for (Element mapperElement : mapperElements) {
            // 获得mapper配置文件的路径
            String mapperPath = mapperElement.attributeValue("resource");
            InputStream mapperInputSteam = Resources.getResourceAsSteam(mapperPath);

            mapperBuilder.parse(mapperInputSteam);
        }

        return configuration;
    }
}
