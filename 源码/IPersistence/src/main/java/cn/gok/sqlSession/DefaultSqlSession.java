package cn.gok.sqlSession;

import cn.gok.executor.Executor;
import cn.gok.executor.SimpleExecutor;
import cn.gok.pojo.Configuration;
import cn.gok.pojo.MappedStatement;

import java.lang.reflect.*;
import java.sql.SQLException;
import java.util.List;

public class DefaultSqlSession implements SqlSession {
    private Configuration configuration;
    private Executor simpleExcutor = new SimpleExecutor();

    public DefaultSqlSession(Configuration configuration){
        this.configuration = configuration;
    }

    @Override
    public <E> List<E> selectList(String statementId, Object... param) throws Exception {
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        List<E> query = simpleExcutor.query(configuration, mappedStatement, param);
        return query;
    }

    @Override
    public <T> T selectOne(String statementId, Object... params) throws Exception {
        List<Object> objects = selectList(statementId, params);

        if(objects.size() ==1){
            return (T) objects.get(0);
        }else {
            throw new RuntimeException("返回结果过多");
        }
    }


    @Override
    public int update(String statementId) throws Exception {
        return update(statementId, null);
    }

    @Override
    public int update(String statementId, Object... params) throws Exception {
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        int i = simpleExcutor.update(configuration, mappedStatement, params);
        return i;
    }

    @Override
    public int delete(String statementId) throws Exception {
        return delete(statementId,null);
    }

    @Override
    public int delete(String statementId,Object... params) throws Exception {
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        int i = simpleExcutor.delete(configuration, mappedStatement, params);
        return i;
    }

    @Override
    public int insert(String statementId, Object... params) throws Exception {
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        return simpleExcutor.insert(configuration,mappedStatement,params);
    }

    @Override
    public void close() throws SQLException {
        simpleExcutor.close();
    }

    @Override
    public <T> T getMappper(Class<?> mapperClass) {

        InvocationHandler invocationHandler = new InvocationHandler(){
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

                String methodName = method.getName();
                // className:namespace
                String className = method.getDeclaringClass().getName();
                //statementid
                String key = className+"."+methodName;

                MappedStatement mappedStatement = configuration.getMappedStatementMap().get(key);
                String methodType = configuration.getMethodType().get(key);

                Type genericReturnType = method.getGenericReturnType();

                Object result = null;

                switch (methodType){
                    case "UPDATE":
                        result = update(key,args);break;
                    case "INSERT":
                        result = insert(key,args);break;
                    case "DELETE":
                        result = delete(key,args);break;
                    case "SELECT":
                        if(genericReturnType instanceof ParameterizedType){
                            result = selectList(key,args);
                        }else{
                            result = selectOne(key,args);
                        }
                }
                return result;
            }
        };

        T o = (T) Proxy.newProxyInstance(mapperClass.getClassLoader(),new Class[]{mapperClass},invocationHandler);

        return o;
    }
}
