package cn.gok.sqlSession;

import cn.gok.config.XMLConfigBuilder;
import cn.gok.pojo.Configuration;
import org.dom4j.DocumentException;

import java.beans.PropertyVetoException;
import java.io.InputStream;

/**
 * 用来构建SQLSessionFactory对象
 */
public class SqlSessionFactoryBuilder {

    /**
     * 获取SqlSessionFactory
     */
    public SqlSessionFactory build(InputStream in) throws PropertyVetoException, DocumentException, ClassNotFoundException {
        //1、利用Dom4j 解析配置文件 ，并封装到我们定义好的Configuration对象中
        //   对配置文件的解析通过我们自定义的工具类XMLConfigBuilder来实现
        XMLConfigBuilder configBuilder = new XMLConfigBuilder();
        Configuration configuration = configBuilder.parseConfig(in);


        //2、创建SqlSessionFactory对象
        SqlSessionFactory sqlSessionFactory = new DefaultSqlSessionFactory(configuration);
        return sqlSessionFactory;
    }
}
