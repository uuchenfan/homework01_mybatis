package cn.gok.sqlSession;

import java.sql.SQLException;
import java.util.List;

/**
 * SqlSession 可以实现对数据库相关操作
 */
public interface SqlSession {

    /**
     *  根据条件查询多个数据的方法
     * @param statementId ：  mapper.xml 配置文件中的对应的sql标签的statementId，由namespace + sql标签Id组成
     * @param param      ：   查询时的指定的条件
     * @param <E>       ：     返回的的数据类型
     * @return
     * @throws Exception
     */
    public <E> List<E> selectList(String statementId, Object... param) throws Exception;

    /**
     * 根据条件查询单个数据的方法
     * @param statementId ： mapper.xml 配置文件中的对应的sql标签的statementId，由namespace + sql标签Id组成
     * @param params  ：    查询时的指定的条件
     * @param <T> ：       返回的的数据类型
     * @return
     * @throws Exception
     */
    public <T> T selectOne(String statementId,Object... params) throws Exception;


    int update(String statementId) throws Exception;

    /**
     * 修改数据
     * @param statementId
     * @param params
     * @return
     * @throws Exception
     */
    int update(String statementId,Object... params) throws Exception;

    int delete(String statementId) throws Exception;

    /**
     * 删除数据
     * @param statementId
     * @param params
     * @return
     * @throws Exception
     */
    int delete(String statementId,Object... params) throws Exception;

    /**
     * 插入数据
     * @param statementId
     * @param params
     * @return
     * @throws Exception
     */
    int insert(String statementId,Object... params) throws Exception;


    /**
     *  关闭连接
     * @throws SQLException
     */
    public void close() throws SQLException;


    <T> T getMappper(Class<?> mapperClass);
}
