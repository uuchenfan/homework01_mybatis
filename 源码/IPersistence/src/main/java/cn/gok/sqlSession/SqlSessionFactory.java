package cn.gok.sqlSession;

/**
 * 用来创建SqlSession对象的工厂类
 */
public interface SqlSessionFactory {
    /**
        获取SqlSession的方法
     */
    public SqlSession openSession();
}
