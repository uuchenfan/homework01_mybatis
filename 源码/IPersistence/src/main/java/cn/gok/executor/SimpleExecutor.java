package cn.gok.executor;

import cn.gok.config.BoundSql;
import cn.gok.pojo.Configuration;
import cn.gok.pojo.MappedStatement;
import cn.gok.utils.GenericTokenParser;
import cn.gok.utils.ParameterMapping;
import cn.gok.utils.ParameterMappingTokenHandler;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SimpleExecutor implements Executor {

    private Connection connection = null;

    @Override
    public <E> List<E> query(Configuration configuration, MappedStatement mappedStatement, Object[] param) throws Exception {

        //1、获取数据库连接
       connection = configuration.getDataSource().getConnection();

        //2、获取要执行的sql语句
        String sql = mappedStatement.getSql();

        //3、对sql语句进行处理，并获取解析结果对象
        BoundSql boundSql = getBoundSql(sql);

        //4、获取解析的最终sql语句
        String sqlText = boundSql.getSqlText();

        //5、获取查询sql标签中，指定的查询参数类型
        Class<?> paramterType = mappedStatement.getParamterType();

        //6、获取PreparedStatement对象
        PreparedStatement preparedStatement = connection.prepareStatement(sqlText);

        /*
         7、设置PreparedStatement中占位符的数据，现在的sql可能是： select * from t_user where user_name = ? and password = ?
            而在BoundSql对象中已经存放了有哪些参数了
         */
        List<ParameterMapping> parameterMappingList = boundSql.getParameterMappingList();

       // 8、遍历参数集合，将他们设置到对应的占位符位置上
        for (int i = 0; i < parameterMappingList.size(); i++) {
            ParameterMapping parameterMapping = parameterMappingList.get(i);
            // 获取到参数的名字
            String name = parameterMapping.getContent();

            //获取对应类的 属性的 Field对象
            Field declaredField = paramterType.getDeclaredField(name);
            //打破私有封装
            declaredField.setAccessible(true);

            // 获取该参数的具体值
            Object o = declaredField.get(param[0]);
            //给占位符设置值
            preparedStatement.setObject(i+1,o);
        }

        // 9、执行sql语句
        ResultSet resultSet = preparedStatement.executeQuery();

        //10、解析结果集
        // 指定的结果类型的Class
        Class<?> resultType = mappedStatement.getResultType();

        // 用来存放得到多个结果集对象
        ArrayList<E> results = new ArrayList<E>();

        while(resultSet.next()){
            //获取结果集元数据
            ResultSetMetaData metaData = resultSet.getMetaData();
            //根据指定结果集类型创建相应对象
            E o = (E) resultType.newInstance();

            int columnCount = metaData.getColumnCount();
            for (int i = 1; i <= columnCount; i++) {
                // 属性名
                String columnName = metaData.getColumnName(i);
                //属性值
                Object value = resultSet.getObject(columnName);

                /*
                   将得到的属性值设置到 刚才创建的对象o 中
                         方式一： 利用反射获取对应属性对象Field ，打破封装， 并修改值

                         方式二： 创建属性描述器PropertyDescriptor，为属性生成读写方法，设置修改值
                 */
                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(columnName, resultType);
                //获取写方法
                Method writeMethod = propertyDescriptor.getWriteMethod();
                // 将值写入
                writeMethod.invoke(o,value);
            }

            results.add(o);
        }
        return results;
    }

    @Override
    public int update(Configuration configuration, MappedStatement mappedStatement, Object[] param) throws Exception {

        //1、获取数据库连接
        connection = configuration.getDataSource().getConnection();

        //2、获取要执行的sql语句
        String sql = mappedStatement.getSql();

        //3、对sql语句进行处理，并获取解析结果对象
        BoundSql boundSql = getBoundSql(sql);

        //4、获取解析的最终sql语句
        String sqlText = boundSql.getSqlText();

        //5、获取查询sql标签中，指定的查询参数类型
        Class<?> paramterType = mappedStatement.getParamterType();

        //6、获取PreparedStatement对象
        PreparedStatement preparedStatement = connection.prepareStatement(sqlText);

        /*
         7、设置PreparedStatement中占位符的数据，现在的sql可能是： update t_user set password = ? where user_name = ?
            而在BoundSql对象中已经存放了有哪些参数了
         */
        List<ParameterMapping> parameterMappingList = boundSql.getParameterMappingList();

        // 8、遍历参数集合，将他们设置到对应的占位符位置上
        for (int i = 0; i < parameterMappingList.size(); i++) {
            //param 为空表示没有输入 ，入参，则直接给所有占位符赋值为null
            if(param == null){
                preparedStatement.setObject(i+1,null);
            }else {
                ParameterMapping parameterMapping = parameterMappingList.get(i);
                // 获取到参数的名字
                String name = parameterMapping.getContent();

                //获取对应类的 属性的 Field对象
                Field declaredField = paramterType.getDeclaredField(name);

                //打破私有封装
                declaredField.setAccessible(true);

                // 获取该参数的具体值
                Object o  = declaredField.get(param[0]);

                //给占位符设置值
                preparedStatement.setObject(i+1,o);
            }
        }

        // 9、执行sql修改操作
        int i = preparedStatement.executeUpdate();
        return i;
    }


    @Override
    public int insert(Configuration configuration, MappedStatement mappedStatement, Object[] param) throws Exception {

        //1、获取数据库连接
        connection = configuration.getDataSource().getConnection();

        //2、获取要执行的sql语句
        String sql = mappedStatement.getSql();

        //3、对sql语句进行处理，并获取解析结果对象
        BoundSql boundSql = getBoundSql(sql);

        //4、获取解析的最终sql语句
        String sqlText = boundSql.getSqlText();

        //5、获取查询sql标签中，指定的查询参数类型
        Class<?> paramterType = mappedStatement.getParamterType();

        //6、获取PreparedStatement对象
        PreparedStatement preparedStatement = connection.prepareStatement(sqlText);

        /*
         7、设置PreparedStatement中占位符的数据，现在的sql可能是： update t_user set password = ? where user_name = ?
            而在BoundSql对象中已经存放了有哪些参数了
         */
        List<ParameterMapping> parameterMappingList = boundSql.getParameterMappingList();

        // 8、遍历参数集合，将他们设置到对应的占位符位置上
        for (int i = 0; i < parameterMappingList.size(); i++) {
            ParameterMapping parameterMapping = parameterMappingList.get(i);
            // 获取到参数的名字
            String name = parameterMapping.getContent();

            //获取对应类的 属性的 Field对象
            Field declaredField = paramterType.getDeclaredField(name);
            //打破私有封装
            declaredField.setAccessible(true);

            // 获取该参数的具体值
            Object o = declaredField.get(param[0]);
            //给占位符设置值
            preparedStatement.setObject(i+1,o);
        }

        // 9、执行sql修改操作
        int i = preparedStatement.executeUpdate();
        return i;
    }

    @Override
    public int delete(Configuration configuration, MappedStatement mappedStatement, Object[] param) throws Exception {

        //1、获取数据库连接
        connection = configuration.getDataSource().getConnection();

        //2、获取要执行的sql语句
        String sql = mappedStatement.getSql();

        //3、对sql语句进行处理，并获取解析结果对象
        BoundSql boundSql = getBoundSql(sql);

        //4、获取解析的最终sql语句
        String sqlText = boundSql.getSqlText();

        //5、获取查询sql标签中，指定的查询参数类型
        Class<?> paramterType = mappedStatement.getParamterType();

        //6、获取PreparedStatement对象
        PreparedStatement preparedStatement = connection.prepareStatement(sqlText);

        /*
         7、设置PreparedStatement中占位符的数据，现在的sql可能是： update t_user set password = ? where user_name = ?
            而在BoundSql对象中已经存放了有哪些参数了
         */
        List<ParameterMapping> parameterMappingList = boundSql.getParameterMappingList();

        // 8、遍历参数集合，将他们设置到对应的占位符位置上
        for (int i = 0; i < parameterMappingList.size(); i++) {
            //param 为空表示没有输入 ，入参，则直接给所有占位符赋值为null
            if(param == null){
                preparedStatement.setObject(i+1,null);
            }else {
                ParameterMapping parameterMapping = parameterMappingList.get(i);
                // 获取到参数的名字
                String name = parameterMapping.getContent();

                //获取对应类的 属性的 Field对象
                Field declaredField = paramterType.getDeclaredField(name);

                //打破私有封装
                declaredField.setAccessible(true);

                // 获取该参数的具体值
                Object o  = declaredField.get(param[0]);

                //给占位符设置值
                preparedStatement.setObject(i+1,o);
            }
        }

        // 9、执行sql修改操作
        int i = preparedStatement.executeUpdate();
        return i;
    }

    @Override
    public void close() throws SQLException {
        connection.close();
    }


    /**
     * 对sql语句进行解析的方法
     * @param sql 要解析的sql
     * @return  将解析后的结果封装到BoundSql对象中返回
     */
    private BoundSql getBoundSql(String sql) {
        // 标记处理类： 主要配合通用标记解析器GenericTokenParser类完成对配置文件等的解析工作，
        ParameterMappingTokenHandler parameterMappingTokenHandler = new ParameterMappingTokenHandler();

        /*
         * GenericTokenParser : 通用的标记解析器， 完成了对代码片段中的占位符的解析，然后再根据给定的标记处理器TokenHandler来进行表达式的处理
         *
         *      其构造器的三个参数分别为：openToken ： 开始标记
         *                              closeToken： 结束标记
         *                              handler： 标记处理器
         */
        GenericTokenParser genericTokenParser = new GenericTokenParser("#{","}",parameterMappingTokenHandler);
        String parse = genericTokenParser.parse(sql);
        List<ParameterMapping> parameterMappings = parameterMappingTokenHandler.getParameterMappings();

        BoundSql boundSql = new BoundSql(parse, parameterMappings);
        return boundSql;
    }
}
