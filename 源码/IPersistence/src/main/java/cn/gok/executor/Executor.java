package cn.gok.executor;

import cn.gok.pojo.Configuration;
import cn.gok.pojo.MappedStatement;

import java.sql.SQLException;
import java.util.List;

public interface Executor {
    /**
     * 查询方法，从数据库中查询出数据，SqlSession 中的查询方法实际是调用这里
     * @param configuration ： 全局配置对象
     * @param mappedStatement ：该mappedStatement对象代表着mapper配置文件中 某条select的sql标签的相关信息
     *                        如select的sql语句，入参，返回数据类型等
     * @param param ： 查询条件
     * @param <E>
     * @return       返回查询的结果集
     * @throws Exception
     */
    <E> List<E> query(Configuration configuration, MappedStatement mappedStatement, Object[] param) throws Exception;

    /**
     * 修改方法
     * @param configuration   全局配置对象
     * @param mappedStatement  该mappedStatement对象代表着 mapper配置文件中某条update的sql标签的相关信息
     *                          如update的sql语句，入参，返回数据类型等
     * @param param  修改数据时的条件
     * @return       返回修改的条目数
     * @throws Exception
     */
    int update(Configuration configuration, MappedStatement mappedStatement, Object[] param)throws Exception;


    /**
     * 插入数据方法
     * @param configuration   全局配置对象
     * @param mappedStatement  该mappedStatement对象代表着 mapper配置文件中某条insert的sql标签的相关信息
     *                         如insert的sql语句，入参，返回数据类型等
     * @param param        插入的具体字段
     * @return
     * @throws Exception
     */
    int insert(Configuration configuration, MappedStatement mappedStatement, Object[] param)throws Exception;

    /**
     * 删除数据方法
     * @param configuration   全局配置对象
     * @param mappedStatement  该mappedStatement对象代表着 mapper配置文件中某条delete的sql标签的相关信息
     *                         如delete的sql语句，入参，返回数据类型等
     * @param param
     * @return
     * @throws Exception
     */
    int delete(Configuration configuration, MappedStatement mappedStatement, Object[] param)throws Exception;

    void close() throws SQLException;
}
