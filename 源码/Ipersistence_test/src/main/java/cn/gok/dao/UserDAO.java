package cn.gok.dao;

import cn.gok.entity.User;

import java.util.List;

public interface UserDAO {

    User findUserById(User user);

    List<User> findAll(User user);
    int saveUser(User user);
    int deleteUserById(User user);
    int updateUserById(User user);
}
