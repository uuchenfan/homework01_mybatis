import cn.gok.dao.UserDAO;
import cn.gok.entity.User;
import cn.gok.io.Resources;
import cn.gok.sqlSession.SqlSession;
import cn.gok.sqlSession.SqlSessionFactory;
import cn.gok.sqlSession.SqlSessionFactoryBuilder;
import org.dom4j.DocumentException;
import org.junit.Before;
import org.junit.Test;

import java.beans.PropertyVetoException;
import java.io.InputStream;
import java.util.List;

public class SqlSessionTest {
    private SqlSession sqlSession;
    @Before
    public void init() throws DocumentException, PropertyVetoException, ClassNotFoundException {
        InputStream inputStream = Resources.getResourceAsSteam("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        sqlSession = sqlSessionFactory.openSession();
    }


    @Test
    public void testSelectOne() throws Exception {
        User user = new User();
        user.setId(1);
        User userx  = (User)sqlSession.selectOne("cn.gok.dao.UserDAO.findUserById", user);
        System.out.println(userx);
    }

    @Test
    public void testSelectList() throws Exception {
        User user = new User();
        user.setId(2);
        List<User> users  = sqlSession.selectList("cn.gok.dao.UserDAO.findAll", user);
        for (User u :users) {
            System.out.println(u);
        }
    }


    @Test
    public void testUpdate() throws Exception {
        User user = new User();
        user.setId(1);
        user.setPassword("3333333");
        int i = sqlSession.update("cn.gok.dao.UserDAO.updateUserById", user);
        System.out.println(i);
    }

    @Test
    public void testDelete() throws Exception {
        User user = new User();
        user.setId(3);
        int i = sqlSession.delete("cn.gok.dao.UserDAO.deleteUserById",user);
        System.out.println(i);
    }

    @Test
    public void testInsert() throws Exception {
        User user = new User(7,"userx","123446");
        int i = sqlSession.insert("cn.gok.dao.UserDAO.saveUser",user);
        System.out.println(i);
    }

    @Test
    public void testMapperSelectOne(){
        UserDAO userDAO = sqlSession.getMappper(UserDAO.class);

        User user = new User();
        user.setId(1);
        User userById = userDAO.findUserById(user);
        System.out.println(userById);
    }

    @Test
    public void testMapperSelectList(){
        UserDAO userDAO = sqlSession.getMappper(UserDAO.class);
        User user = new User();
        user.setId(2);
        List<User> users = userDAO.findAll(user);
        for (User u :users) {
            System.out.println(u);
        }
    }


    @Test
    public void testMapperUpdate(){
        UserDAO userDAO = sqlSession.getMappper(UserDAO.class);
        User user = new User();
        user.setId(1);
        user.setPassword("user12345");
        int i = userDAO.updateUserById(user);
        System.out.println(i);
    }


    @Test
    public void testMapperInsert(){
        UserDAO userDAO = sqlSession.getMappper(UserDAO.class);
        User user = new User();
        user.setId(8);
        user.setUsername("user8");
        user.setPassword("112233");
        userDAO.saveUser(user);
    }


    @Test
    public void testMapperDelete(){
        UserDAO userDAO = sqlSession.getMappper(UserDAO.class);
        User user = new User();
        user.setId(8);
        userDAO.deleteUserById(user);
    }
}
